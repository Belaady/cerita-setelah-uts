from django.urls import path
from .views import landingPage, loginView, logoutView


app_name = 'cerita9app'

urlpatterns = [
	path('', landingPage, name='landingPage'),
    path('login/', loginView, name='login' ),
    path('logout/', logoutView, name='logout'),

	]
