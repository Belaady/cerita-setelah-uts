from django.test import TestCase
from django.contrib.auth import authenticate, login, logout
from django.test import Client, LiveServerTestCase
from selenium.webdriver.chrome.options import Options
from .views import *
from django.urls import resolve, reverse
from selenium import webdriver
import time
import unittest

# Create your tests here.

class AuthAppTest(TestCase):
    def test_url_s9_exist(self):
        response = Client().get('/cerita9/')
        self.assertEqual(response.status_code, 200)
    def test_url_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)
    def test_s9app_using_template(self):
        response = Client().get('/cerita9/')

        self.assertTemplateUsed(response, 'landingPage.html')
    
    
    def test_auth_app_using_landingPage_func(self):
        found = resolve('/cerita9/')
        self.assertEqual(found.func, landingPage)
    
    def test_cannot_access_logout_with_no_auth(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 404)

    
    # def test_can_access_login_with_no_auth_using_template(self):
    #     response = Client().get('cerita9/login/')
    #     self.assertTemplateUsed(response, 'login.html')

    # def test_can_access_login_with_no_auth_using_login_view_func(self):
    #     found = resolve('cerita9/login/')
    #     self.assertEqual(found.func, loginView)



# Functional Test
class FunctionalTestS9(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        super(FunctionalTestS9, self).setUp()
        

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTestS9, self).tearDown()

    def test_title_element_in_project(self):
        self.browser.get(self.live_server_url + "/cerita9/")

        title = self.browser.find_element_by_tag_name('h1')
        self.assertIn("Welcome", title.text)
        time.sleep(10)
    


    def test_can_login(self):
        # Create user
        User.objects.create_user(username='bela', email='bela@bela.bela', password='test123')

        self.browser.get(self.live_server_url + "/cerita9/login/")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('bela')
        password.send_keys('test123')
        submit.click()
        time.sleep(2)

        self.assertIn("Welcomeback bela", self.browser.page_source)
        time.sleep(2)
    
    def test_can_signup(self):
        self.browser.get(self.live_server_url + "/cerita9/login/")

        title = self.browser.find_element_by_id('title_signup')
        username = self.browser.find_element_by_id('username_signup')
        email = self.browser.find_element_by_id('email_signup')
        password = self.browser.find_element_by_id('password_signup')
        submit = self.browser.find_element_by_id('submit_signup')

        title.click()
        username.send_keys('bela')
        email.send_keys('bela@bela.bela')
        password.send_keys('test123')
        submit.click()
        time.sleep(2)

        self.assertIn("Welcomeback bela", self.browser.page_source)
        time.sleep(2)

    def test_auth_can_access_logout(self):
        # Create user
        User.objects.create_user(username='bela', email='bela@bela.bela', password='test123')

        self.browser.get(self.live_server_url + "/cerita9/login/")


        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('bela')
        password.send_keys('test123')
        submit.click()
        time.sleep(2)

        logout = self.browser.find_element_by_id('logout')
        logout.click()
        time.sleep(2)

        self.assertIn("Welcome", self.browser.page_source)
        time.sleep(2)


    def test_auth_cannot_access_login(self):
        # Create user
        User.objects.create_user(username='bela', email='bela@bela.bela', password='test123')

        self.browser.get(self.live_server_url + "/cerita9/login/")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('bela')
        password.send_keys('test123')
        submit.click()
        time.sleep(2)

        self.browser.get(self.live_server_url + "/cerita9/login/")

        self.assertIn("Welcomeback bela", self.browser.page_source)
        time.sleep(2)





