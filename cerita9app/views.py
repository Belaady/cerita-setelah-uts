from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
def landingPage(request):
    context = { 
        'greeting' : 'Welcome',
        'please' : 'Do you have an account?',
        'log' : 'login',
    }
    
    if request.user.is_authenticated:
        context['greeting'] = 'Welcomeback ' + request.session["username"]
        context['please'] = 'Please Log Out'
        context['log'] = 'logout'

    return render(request, 'landingPage.html', context)


def loginView(request):
    if request.method == "GET":
        if request.user.is_authenticated:
            return redirect('/cerita9/')
        else:
            return render(request, 'login.html')

    if request.method == "POST":
        if len(request.POST) == 3:
            username_login = request.POST['username']
            password_login = request.POST['password']
                    
            user = authenticate(request, username=username_login, password=password_login)
            
            if user is not None:
                login(request, user)
                request.session['username'] = user.username
                return redirect('/cerita9/')
            else:
                return redirect('login')
        elif len(request.POST) == 4:
            username_login = request.POST['username']
            email_login = request.POST['email']
            password_login = request.POST['password']

            users = User.objects.all()

            for user in users:
                if user.username == username_login :
                    return redirect('login')

            user = User.objects.create_user(username=username_login, email=email_login, password=password_login)
            login(request, user)
            request.session["username"] = user.username
            return redirect('/cerita9/')

 
@login_required
def logoutView(request):
    request.session.flush()
    if request.method == "GET":
        if request.user.is_authenticated:
            logout(request)
    return redirect('/cerita9/')
