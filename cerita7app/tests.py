from django.test import TestCase
from django.test import Client, LiveServerTestCase
from selenium.webdriver.chrome.options import Options
from .views import *
from django.urls import resolve
from selenium import webdriver
import time
import unittest

# Create your tests here.
class s7UnitTest(TestCase):
    
    def test_url_home_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    def test_url_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)
    def test_view_s7(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    
class FunctionalTest(LiveServerTestCase):
    def setUp(self): 
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrive_it_later(self):
        self.browser.get(self.live_server_url+'/')
        self.assertIn("Sinta Bela Damayanti", self.browser.page_source)
        time.sleep(1)

    def test_accordion_exist(self):
        self.browser.get(self.live_server_url+'/')
        self.assertIn("accordion", self.browser.page_source)
        time.sleep(1)

    def test_accordion_act_onclick(self):
        self.browser.get(self.live_server_url+'/')
        time.sleep(1)
        self.browser.find_element_by_id("act").click()
        time.sleep(1)
        self.assertIn("Belajar", self.browser.page_source)


    def test_accordion_org_onclick(self):
        self.browser.get(self.live_server_url+'/')
        time.sleep(1)
        self.browser.find_element_by_id("org").click()
        time.sleep(1)
        self.assertIn("HRD of Direct Marketing Compfest", self.browser.page_source)
      
        
    def test_accordion_ach_onclick(self):
        self.browser.get(self.live_server_url+'/')
        time.sleep(1)
        self.browser.find_element_by_id("ach").click()
        time.sleep(1)
        self.assertIn("otw", self.browser.page_source)





    

    