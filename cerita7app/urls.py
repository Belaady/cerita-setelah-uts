from django.urls import path
from .views import home

app_name = 'cerita7app'

urlpatterns = [
	path('', home, name='home'),
	]