
$('.toggle').click(function(e) {
    e.preventDefault();

  var $this = $(this);

  if ($this.next().hasClass('show')) {
      $this.next().removeClass('show');
      $this.next().slideUp(350);
  } else {
      $this.parent().parent().find('li .inner').removeClass('show');
      $this.parent().parent().find('li .inner').slideUp(350);
      $this.next().toggleClass('show');
      $this.next().slideToggle(350);
  }
});

$('#changeDark').click(function(e) {

  $("head").append('<style type="text/css"></style>');
  var newStyleElement = $("head").children(':last');
  newStyleElement.html("body{font-size: 16px;font-family: 'McLaren', cursive;;width: 100%;height: 100%;margin: 0;color: white;background-color: black;}");

  $("head").append('<style type="text/css"></style>');
  var newStyleElement = $("head").children(':last');
  newStyleElement.html(".desc{padding: 0 20px;margin-top: 100px;text-align: center;color: white;background-color: rgb(38, 52, 58);}");

  $("head").append('<style type="text/css"></style>');
  var newStyleElement = $("head").children(':last');
  newStyleElement.html(".desc-section:after{content: '';position: absolute;bottom: 0;left: 0;right: 0;height: 150px;z-index: -1;background-color: black;}");

  $("head").append('<style type="text/css"></style>');
  var newStyleElement = $("head").children(':last');
  newStyleElement.html(".desc-section{padding-top: 200px;position: relative;background-color: black;}");
});

$('#changeLight').click(function(e) {

  $("head").append('<style type="text/css"></style>');
  var newStyleElement = $("head").children(':last');
  newStyleElement.html("body{font-size: 16px;font-family: 'McLaren', cursive;;width: 100%;height: 100%;margin: 0;color: #333;background-color: rgba(120, 150, 180)}");

  $("head").append('<style type="text/css"></style>');
  var newStyleElement = $("head").children(':last');
  newStyleElement.html(".desc{padding: 0 20px;margin-top: 100px;text-align: center;color: black;background-color: white;}");

  $("head").append('<style type="text/css"></style>');
  var newStyleElement = $("head").children(':last');
  newStyleElement.html(".desc-section:after{content: '';position: absolute;bottom: 0;left: 0;right: 0;height: 150px;z-index: -1;background-color: white;}");

  $("head").append('<style type="text/css"></style>');
  var newStyleElement = $("head").children(':last');
  newStyleElement.html(".desc-section{padding-top: 200px;position: relative;background-color: rgb(126, 191, 255);;}");
});