from django.urls import path
from .views import bookPage

app_name = 'cerita8app'

urlpatterns = [
	path('', bookPage, name='bookPage'),
	]
