from django.shortcuts import render

# Create your views here.
def bookPage(request):
    response = {}
    return render(request, 'bookPage.html', response)