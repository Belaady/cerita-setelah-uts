from django.test import TestCase
from django.test import Client, LiveServerTestCase
from selenium.webdriver.chrome.options import Options
from .views import *
from django.urls import resolve
from selenium import webdriver
import time
import unittest
# Create your tests here.
class s8UnitTest(TestCase):
    
    def test_url_bookPage_exist(self):
        response = Client().get('/cerita8/')
        self.assertEqual(response.status_code, 200)
    def test_url_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)
    def test_view_s8(self):
        found = resolve('/cerita8/')
        self.assertEqual(found.func, bookPage)

class FunctionalTest(LiveServerTestCase):
    
        #setup selenium browser
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)

        #set tearing down
    def tearDown(self):
        self.browser.quit()

        #test opening the web
    def test_open(self):
        self.browser.get(self.live_server_url+'/cerita8/ ')
        self.assertIn("Search your favorite books here!", self.browser.page_source)
        time.sleep(1)
        

