$(document).ready(function(){
  cari("bintang")
  $("#search").on("keyup", function(e){
      book = e.currentTarget.value.toLowerCase()
      cari(book)
});
});

function cari(book){
  var delay = 3000;
    $.ajax({
      method: "GET",
      url: "https://www.googleapis.com/books/v1/volumes?q="+book,
      dataType:"json",
      success: function(result){
        setTimeout(function(){
          $("table").empty();
          $("table").append(
            '<thead>' +
              '<tr>' +
                '<th scope="col">Book</th>' +
                '<th scope="col">Title</th>' +
                '<th scope="col">Author</th>' +
              '</tr>'+
            '</thead>'
          );
          for(i = 0; i < result.items.length ; i++){
            $("table").append(
              "<tbody>" +
                "<tr>" +
                  "<th scope='row'>" + "<img src= '"+ result.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
                  "<td>" + result.items[i].volumeInfo.title + "</td>" +
                  "<td>" + result.items[i].volumeInfo.authors + "</td>" +
                "</tr>" +
              "</tbody>"
            );
          }
        },delay);
      }
    })
  }